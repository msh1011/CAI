from selenium import webdriver
import time
import json
import re

urls = [1378, 1, 3, 4, 5, 2224, 9757, 1092, 1386, 1534, 788, 1434, 498]
card_i = 0
cards = []

for url_item in range(len(urls)):
    print("{}/{}".format(url_item + 1, len(urls)))
    driver = webdriver.Chrome()
    t = driver.get("https://azala.info/static/index.html?deck=" + str(urls[url_item]))
    time.sleep(2)

    range = "1 - 16"
    type = "A"
    css_selector = "div.react-card.white"

    while True:
        a = driver.find_elements_by_css_selector(css_selector)
        for x in a:
            card = {"id": card_i, "cardType": type, "text": x.text}
            card_i += 1
            cards.append(card)
        t = driver.find_elements_by_css_selector(
            "div.btn.react-std-btn.single-icon-btn"
        )
        t[4].click()
        while True:
            f = driver.find_elements_by_css_selector("div.label")
            if f[1].text != range:
                break
        range = f[1].text
        if range.startswith("1 - ") and type == "A":
            t = driver.find_elements_by_css_selector("div.btn.react-std-btn")
            t[4].click()
            type = "Q"
            css_selector = "div.react-card.black"
            time.sleep(0.2)
        elif range.startswith("1 - "):
            break

    driver.quit()


def deduplicate(items):
    seen = set()
    for item in items:
        if not item["text"] in seen:
            if item["cardType"] == "Q":
                pattern = re.compile(".* __* .* __*.*")
                if pattern.match(item["text"]):
                    continue
            seen.add(item["text"])
            yield item


uni_cards = []
for item in deduplicate(cards):
    uni_cards.append(item)

with open("cards.json", "w") as fp:
    json.dump(uni_cards, fp, indent=4)
