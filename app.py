from flask import Flask, send_file, request, jsonify, Response
from flask_socketio import SocketIO, join_room, leave_room, rooms
from active_game import a_game
import random
import string
import os
import re
import importlib
import sys
import signal
import pickle
import json
import pdb

app = Flask(__name__, static_folder="client/build")
socketio = SocketIO(app)
# try:
#     with open('data.pkl', 'rb') as fp:
#         active_games = pickle.load(fp)
# except FileNotFoundError:
#     active_games = {}
# 
# def handler(signal, frame):
#     with open('data.pkl', 'wb') as fp:
#         pickle.dump(active_games, fp)
#     sys.exit(0)
# 
# signal.signal(signal.SIGINT, handler)
active_games = {}

gamemodes = {}
gamemode_folder = {}
for x in [name for name in os.listdir(".") if os.path.isdir(name)]:
    if any(i == "game.py" for i in os.listdir(x)):
        game = importlib.import_module("{}.game".format(x))
        gamemodes[game.KEY] = game
        gamemode_folder[game.KEY] = x

def make_room_code():
    global active_games
    text = ""
    while text in active_games or not text:
        text = ""
        for x in range(4):
            text += random.choice(string.ascii_uppercase)
    return text


def is_valid_code(code):
    return re.match(r"[A-Z]{4}", str(code)) and code in active_games


@app.route("/api/games", methods=["GET"])
def get_games():
    games = []
    code = request.cookies.get("code")
    uid = request.cookies.get("uid")
    if code is not None and is_valid_code(code):
        if active_games[code].leader_uid == uid:
            for k, v in gamemodes.items():
                games.append({"name": v.NAME, "key": v.KEY})
    return jsonify(data=games)


@app.route("/api/players", methods=["GET"])
def get_players():
    code = request.cookies.get("code")
    if code is not None and is_valid_code(code):
        return jsonify(data=[x["name"] for x in active_games[code].players])
    return jsonify(error="Invalid Code"), 400


@app.route("/api/create", methods=["GET"])
def create():
    code = make_room_code()
    active_games[code] = a_game(code)
    out = jsonify(data=code)
    out.set_cookie("code", code)
    out.set_cookie("role", "master")
    
    return out

@socketio.on('room')
def on_join(room):
    join_room(room)
    active_games[room].update_players()

@socketio.on('leave')
def on_leave(data):
    room = data['room']
    leave_room(room)


@app.route("/api/start", methods=["GET"])
def set_game():
    global active_games
    gamemode = request.args.get("game", default="", type=str).upper()
    debug = request.args.get("debug", default="", type=str) != ""
    code = request.cookies.get("code")
    uid = request.cookies.get("uid")

    if code is None or not is_valid_code(code) or code not in active_games:
        return jsonify(error="Invalid game code"), 400
    if active_games[code].active:
        return jsonify(error="Game is active"), 400
    if active_games[code].leader_uid != uid:
        return jsonify(error="You cant do that"), 400
    if gamemode not in gamemodes:
        return jsonify(error="Invalid gamemode"), 400
    if gamemodes[gamemode].MIN_PLAYERS > len(active_games[code].players):
        return jsonify(error="Not enough players to start game"), 400
    active_games[code].game = gamemode
    active_games[code].game_obj = gamemodes[gamemode].create(active_games[code].players, code, debug=debug)
    active_games[code].active = True
    active_games[code].send_refresh()
    return jsonify(data="started")


@app.route("/api/join", methods=["GET"])
def join_game():
    game_code = request.args.get("code", default="", type=str).upper()
    game_name = request.args.get("name", default="", type=str)
    uid = request.cookies.get("uid")
    
    if game_code not in active_games:
        return jsonify(error="Invalid game code"), 401
            
    if any(x["uid"] == uid for x in active_games[game_code].players):
        return jsonify(message="Changed Name")
    if any(x["name"] == game_name for x in active_games[game_code].players):
        return jsonify(error="Name already taken"), 402
    uid = "".join(random.choices(string.ascii_letters + string.digits, k=32))
    active_games[game_code].players.append({"name": game_name, "uid": uid})
    if active_games[game_code].leader_uid is None:
        active_games[game_code].leader_uid = uid
    out = jsonify(message="Joined")
    out.set_cookie("code", game_code)
    out.set_cookie("uid", uid)
    out.set_cookie("role", "player")
    active_games[game_code].update_players()
    return out

@app.route("/api/debug", methods=["GET"])
def degbug():
    pdb.set_trace()

    return str(active_games)


@app.route("/api/game/<string:method>", methods=["GET"])
def pass_function(method):
    code = request.cookies.get("code")
    uid = request.cookies.get("uid")
    if code is None or not is_valid_code(code) or code not in active_games:
        return jsonify(error="Invalid game code"), 400
    if not active_games[code].active:
        return jsonify(error="No active game"), 400
    game = gamemodes[active_games[code].game]
    game_obj = active_games[code].game_obj
    args = request.args.to_dict()
    for x in active_games[code].players:
        if x["uid"] == uid:
            args["game_name"] = x["name"]
    return game.ENDPOINTS[method](game_obj, **args)


@app.route("/", defaults={"path": ""}, methods=["GET"])
@app.route("/<path:path>", methods=["GET"])
def serve(path):
    pages = ["play", "join", "master", ""]
    code = request.cookies.get("code")
    folder = ""
    if code is not None and is_valid_code(code) and active_games[code].active:
        folder = gamemode_folder[active_games[code].game]
    full_dir = os.path.join(os.getcwd(), folder, "client", "dist", path)
    if path in pages:
        return send_file(os.path.join(full_dir, "index.html"))
    if path != "" and os.path.exists(full_dir):
        return send_file(full_dir)
    return jsonify(data="error")


if __name__ == "__main__":
    socketio.run(app, host="0.0.0.0", port=80)
