import os, signal, sys, pytest, time
from sys import path
from selenium import webdriver
from importlib import reload
from server_test import AppInterface
import requests
import urllib
import json
import random
import app

def get_new_web_driver(phone=True):
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    if phone:
        options.add_argument("--window-size=1080,2280")
    else:
        options.add_argument("--window-size=1920,1080")
    driver = webdriver.Chrome(chrome_options=options)
    driver.get("http://localhost:8000/")
    return driver

@pytest.fixture(autouse=True)
def driver():
    random.seed(1)
    return
    from multiprocessing import Process

    server = Process(target=app.app.run, kwargs={'host': "0.0.0.0", 'port': 8000})
    server.start()
    time.sleep(1)
    
    driver = get_new_web_driver(phone=False)
    yield driver
    
    server.terminate()
    server.join()
    reload(app)

    
def element_from_str(s, string):
    return s.find_element_by_xpath("//*[contains(text(), '{}')]".format(string))
    

def wait_for_class_name(c, name, timeout=10):
    for x in range(timeout):
        f = c.find_elements_by_class_name(name)
        if f != []:
            return f
        time.sleep(1)
    assert False
    return []

@pytest.mark.driver
def test_driver_play_game(driver):
    params = urllib.parse.urlencode({"debug": "true"})
    res = requests.put("http://localhost:8000/api/create?" + params)
    code = json.loads(res.content)["data"]
    driver.get("http://localhost:8000/master?code={}".format(code))
    num_players = 4
    players = []
    for x in range(num_players):
        print("Creating Player", x+1)
        c = get_new_web_driver()
        element_from_str(c, "Join Game").click()
        form = wait_for_class_name(c, "join--input")
        form[0].send_keys(code)
        form[1].send_keys("TEST_{}".format(x+1))
        element_from_str(c, "Join").click()
        players.append(c)
    time.sleep(1)
    element_from_str(driver, "Start Game").click()
    time.sleep(1)
    judge = 0
    print("Game Started")
    for w in range(5):
        print("Round:", x+1)
        for c in players:
            if(c == players[judge]):
                continue
            form = wait_for_class_name(c, "card--info")
            print("Player {} played {}".format(players.index(c)+1, form[0].text))
            form[0].click()
            time.sleep(0.2)
            element_from_str(c, "Play Card").click()
        time.sleep(1)
        form = wait_for_class_name(players[judge], "card--info")
        print("Player {} selected {}".format(judge+1, form[0].text))
        form[0].click()
        time.sleep(0.2)
        element_from_str(players[judge], "Play Card").click()
        judge = (judge+1) % num_players
        print("WAITING")
        time.sleep(10)
    