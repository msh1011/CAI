import importlib, json, random, time, sys, pytest, urllib
from os import path
from importlib import reload, import_module
import app
import pdb
from werkzeug.http import parse_cookie

class AppInterface:
    
    def __init__(self, client):
        self.client = client

    def create_game(self):
        res = self.client.get("/api/create")
        code = json.loads(res.data)["data"]
        return code

    def start_game(self):
        params = urllib.parse.urlencode({"game": "CAI", "debug": "True"})
        return json.loads(self.client.get("/api/start?"+params).data)

    def join_game(self, code, name):
        params = urllib.parse.urlencode({"code": code, "name": name})
        res = self.client.get("/api/join?" + params)
        return json.loads(res.data)

    def get_stats(self):
        return json.loads(self.client.get("/api/game/status").data)["data"]

    def get_hand(self):
        return json.loads(self.client.get("/api/game/hand").data)

    def play_card(self, card):
        params = urllib.parse.urlencode({"card": card, "custom":""})
        self.client.get("/api/game/play?" + params)

    def judge_card(self, card):
        params = urllib.parse.urlencode({"card": card})
        return json.loads(self.client.get("/api/game/judge?" + params).data)


@pytest.fixture(autouse=True)
def game():
    app.reset_app()
    server = AppInterface(app.app.test_client())
    print(app.active_games)
    code = server.create_game()
    players = [server]
    for x in range(5):
        p = AppInterface(app.app.test_client())
        p.join_game(code, "TEST_{}".format(x + 1))
        players.append(p)
    yield (players)
    reload(app)

@pytest.mark.dev
def test_basic_round(game):

    players = game

    assert players[1].start_game()["data"] == "started"

    stats = players[0].get_stats()
    assert stats["game_state"] is "1"
    assert len(stats["players"]) is 5

    for x in players[1:]:
        x.play_card(0)

    stats = players[0].get_stats()
    assert stats["game_state"] is "2"
    assert len(stats["white_cards"]) == 4
    players[1].judge_card(0)

    stats = players[0].get_stats()
    assert stats["game_state"] is "1"
    assert stats["players"][stats["last_winner"]]["score"] is "1"

@pytest.mark.check
def test_create_many_games():
    app.reset_app()
    game = AppInterface(app.app.test_client())
    for x in range(100):
        game.create_game()

    assert len(app.active_games) >= 100
    reload(app)

@pytest.mark.check
def test_player_get_hand(game):
    random.seed(2)

    players = game
    players[1].start_game()

    exprected_hand = [
        'Hot squirrel-on-whale action.',
        'Reading the entire End-User License Agreement.',
        'Surprise sex!', 
        'Pabst Blue Ribbon.',
        'Deleting System 32.',
        'When you sit down and the cucumber goes further in than expected. ',
        'Flame body spray by Burger King. The scent of seduction with a hint of flame-broiled meat.'
    ]
    hand = players[1].get_hand()["data"]
    assert hand["hand"] == exprected_hand

@pytest.mark.check
def test_player_many_play_cards(game):
    players = game
    players[1].start_game()
    
    for x in range(10):
        players[1].play_card(1)

    stats = players[0].get_stats()
    assert stats["game_state"] is "1"

    for x in range(10):
        players[2].play_card(1)

    stats = players[0].get_stats()
    assert stats["game_state"] is "1"
    
    for x in [3,4,5]:
        players[x].play_card(1)
    
    time.sleep(1)
    stats = players[0].get_stats()
    assert stats["game_state"] is "2"

@pytest.mark.check
def test_judge_before_all_cards_in(game):
    players = game

    players[2].play_card(1)

    res = players[1].judge_card(1)
    assert res["message"] == "Cant do that now"
    assert res["status"] == 400

    players[3].play_card(1)
    players[4].play_card(1)
    players[5].play_card(1)

    res = players[1].judge_card(1)
    assert res["status"] == 200

@pytest.mark.check
def test_many_turns(game):
    random.seed(3)
    players = game[1::]
    players[0].start_game()
    

    for turn in range(10):
        for x in players:
            x.play_card(1)
        res = players[turn%5].judge_card(0)
        assert res["data"] == "success"
    stats = game[0].get_stats()
    assert stats["judge"] == 0
    assert stats["black_card"] == "I hope rule 34 doesn't apply to __."
