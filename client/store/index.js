import Vuex from 'vuex'
import axios from 'axios'

const createStore = () => {
  return new Vuex.Store({
    modules: {
      master: {
        state: {
          players: []
        },
        mutations: {
          update(state, data) {
            state.players = data
          }
        }
      },
      client: {
        state: {
          games: []
        },
        mutations: {
          get_games(state, data) {
            state.games = data
          }
        }
      }
    }
  })
}

export default createStore
