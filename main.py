import app
import os


app.socketio.run(app.app, host="0.0.0.0", port=int(os.getenv("PORT", "80")))
