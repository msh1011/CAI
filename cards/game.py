from flask import jsonify
from .cards import CAI
from flask_socketio import emit


KEY = "CAI"
MIN_PLAYERS = 3
NAME = "Cards Against The Internet"

game = None

class Player:
    def __init__(self, name, uid, room):
        self.name = name
        self.uid = uid
        self.room = room
        self.hand = []
        self.score = 0

    def add_to_hand(self, card):
        self.hand.append(card)
        
    def send_update(self, game):
        data = {
            "state": game.get_state(),
            "hand": game.player_get_hand(self.uid),
            "black_card": game.get_current_black_card(),
            "judge": game.is_player_judge(self.uid),
            "judge_cards": game.get_current_white_cards(),
        }
        print("update_"+self.uid, self.room, data)
        emit("update_"+self.uid, (data), room=self.room, namespace="/")

    def play_card(self, index):
        return self.hand.pop(index)

    def increase_score(self):
        self.score += 1



def create(players, code, debug=False):

    def get_stats(game):
        data = {
            "game_state": str(game.get_state()),
            "black_card": game.get_current_black_card(),
            "white_cards": game.get_current_white_cards(),
            "judge": game.get_current_judge(),
            "players": game.get_players(),
            "last_winner": game.get_last_winner(),
        }
        print("update_master", code, data)
        emit("update_master", (data), room=code, namespace="/")

    game = CAI(event=get_stats, debug=debug)
    for x in players:
        game.join(Player(x["name"], x["uid"], code))
    game.start_game()
    return game


def player_connect(game, game_name, uid):
    game.connect_player(uid)
    return jsonify(data="success")

def master_connect(game):
    game.update(game)
    return jsonify(data="success")

def player_play_card(game, game_name, uid, card, custom):
    try:
        game.player_play_card(uid, int(card), custom=custom)
    except Exception as e:
        return jsonify(error=e.args[0]), 400
    return jsonify(data="succeed")


def player_judge_card(game, game_name, uid, card):
    try:
        game.player_judge_card(uid, int(card))
    except Exception as e:
        return jsonify(error=e.args[0]), 400
    return jsonify(data="success")


ENDPOINTS = {
    "play": player_play_card,
    "judge": player_judge_card,
    "connect": player_connect,
    "master_connect": master_connect
}
