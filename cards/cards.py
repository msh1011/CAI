import json
import random
import enum
import os
from time import sleep


CUSTOM_CARD_MSG = "(Create your own)"


class State(enum.Enum):
    PRESTART = 0
    CHOOSE = 1
    JUDGE = 2
    POSTROUND = 3


class CAI:
    def __init__(self, event=lambda x: None, debug=False):
        self.players = []
        self.black_deck = []
        self.white_deck = []
        self.last_winner = 0
        self.debug = debug
        self.update = event

        self.judge = -1
        self.current_black_card = ""
        self.current_white_cards = []
        self.players_already_played = []
        self.game_state = State.PRESTART
        self.post_count = 0
        __location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__))
        )
        with open(os.path.join(__location__, "cards.json")) as f:
            data = json.load(f)
        for x in data:
            if x["cardType"] is "A":
                self.white_deck.append(x["text"])
            if x["cardType"] is "Q":
                self.black_deck.append(x["text"])
        random.shuffle(self.black_deck)
        if not self.debug:
            for x in range(0, len(self.white_deck), 33):
                self.white_deck.append(CUSTOM_CARD_MSG)
        random.shuffle(self.white_deck)

    def start_game(self):
        if len(self.players) < 3:
            raise ValueError("Not enough players to start", len(self.players))
        if self.game_state is not State.PRESTART:
            raise ValueError("Game already started")
        self.draw_black_card()
        for q in range(7):
            for x in self.players:
                x.add_to_hand(self.white_deck.pop())
        self.game_state = State.CHOOSE
        self.judge = 0
        self.current_white_cards = []
        self.players_already_played = []
        self.update(self)
        for x in range(len(self.players)):
            self.players[x].send_update(self)

    def join(self, player):
        if self.game_state is not State.PRESTART:
            raise RuntimeError("Game is already Started")

        self.players.append(player)

        return True

    def draw_black_card(self):
        self.current_black_card = self.black_deck.pop()

    def player_play_card(self, uid, card_index, custom=""):
        if self.game_state is not State.CHOOSE:
            raise ValueError("Cant do that now")
        if uid in self.players_already_played:
            raise ValueError("Cant do that now (already played card)")
        player = next((x for x in self.players if x.uid == uid), None)
        if player is None:
            raise ValueError("Invalid UID", uid)
        if self.players.index(player) is self.judge:
            raise ValueError("UID belongs to judge", uid)

        card = player.play_card(card_index)
        if card == CUSTOM_CARD_MSG:
            if custom != "":
                card = custom
            else:
                raise ValueError("Custom Card cant be blank")
        self.current_white_cards.append({"card": card, "player": uid})

        self.players_already_played.append(uid)

        player.add_to_hand(self.white_deck.pop())
        player.send_update(self)

        if len(self.current_white_cards) is len(self.players) - 1:
            random.shuffle(self.current_white_cards)
            self.game_state = State.JUDGE
            self.players[self.judge].send_update(self)
        self.update(self)

    def player_judge_card(self, uid, card_index):
        if self.game_state is not State.JUDGE:
            raise ValueError("Cant do that now")
        player = next((x for x in self.players if x.uid == uid), None)
        if player is None:
            raise ValueError("Invalid UID", uid)
        if self.players.index(player) is not self.judge:
            raise ValueError("UID does not belong to judge", uid)
        winner = self.current_white_cards[card_index]["player"]
        player = next((x for x in self.players if x.uid == winner), None)
        player.increase_score()
        self.last_winner = self.players.index(player)
        self.fill_back_card(self.current_white_cards[card_index]["card"])
        self.game_state = State.POSTROUND
        self.update(self)
        self.new_turn()

    def new_turn(self):
        if not self.debug:
            sleep(6)
        self.judge = (self.judge + 1) % len(self.players)
        self.draw_black_card()
        self.current_white_cards = []
        self.players_already_played = []
        self.game_state = State.CHOOSE
        self.update(self)
        for player in self.players:
            player.send_update(self)
            
    def fill_back_card(self, winner):
        c = self.current_black_card
        try:
            i = c.index("_")
            i1 = (c[:i] + winner + c[i:]).replace("_", "").strip(".")
            self.current_black_card = i1
        except:
            self.current_black_card = c + ": " + winner
        

    def player_get_hand(self, uid):
        if self.game_state is State.PRESTART:
            raise ValueError("Wrond state")
        player = next((x for x in self.players if x.uid == uid), None)
        if player is None:
            raise ValueError("Invalid UID", uid)
        return player.hand
        
    def connect_player(self, uid):
        player = next((x for x in self.players if x.uid == uid), None)
        if player is None:
            raise ValueError("Invalid UID", uid)
        player.send_update(self)

    def get_current_white_cards(self):
        if self.game_state == State.JUDGE or self.game_state == State.POSTROUND:
            return [c["card"] for c in self.current_white_cards]
        else:
            return ["Cards Against the Internet"] * len(self.current_white_cards)

    def get_current_black_card(self):
        return self.current_black_card

    def get_current_judge(self):
        return self.judge

    def is_player_judge(self, uid):
        return "" if self.players[self.judge].uid != uid else "judge"

    def get_state(self):
        return self.game_state.value

    def get_last_winner(self):
        return self.last_winner

    def get_players_already_played(self):
        return self.players_already_played

    def get_players(self):
        data = []
        for x in range(len(self.players)):
            data.append(
                {
                    "name": self.players[x].name,
                    "score": str(self.players[x].score),
                    "is_judge": "",
                    "index": x,
                }
            )
        if self.judge != -1:
            data[self.judge]["is_judge"] = "judge"
        return data
