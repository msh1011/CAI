from flask_socketio import emit


class a_game:
    players = []
    game = None
    game_obj = None
    leader_uid = None
    active = False
    socket = None
    def __init__(self, code):
        self.code = code

    def update_players(self):
        emit("update_players", (self.players), room=self.code, namespace="/")

    def send_refresh(self):
        emit("refresh", room=self.code, namespace="/")
